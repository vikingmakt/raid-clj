package raid.exceptions;

public class EncodeInvalid extends Exception {
    public EncodeInvalid(String encode) {
        super("Encode " + encode + " is invalid");
    }
}
