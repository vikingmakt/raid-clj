package raid.exceptions;

public class ActionMissing extends Exception {
    public ActionMissing() {
        super("All messages must have an :action defined");
    }
}
