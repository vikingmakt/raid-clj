package raid.exceptions;

public class RemoteConnectionClosed extends Exception {
    public RemoteConnectionClosed() {
        super();
    }

    public RemoteConnectionClosed(String message) {
        super(message);
    }
}
