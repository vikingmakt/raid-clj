(defproject br.com.vikingmakt/raid "1.6.0"
  :description "Raid client"
  :url "https://gitlab.com/vikingmakt/raid-clj"
  :license {:name "BSD-2-Clause"}
  :dependencies [[br.com.vikingmakt/augustus "0.1.12"]
                 [clojure-msgpack "1.2.1"]
                 [org.clojure/clojure "1.10.1"]
                 [utcode "0.2.7"]]
  :java-source-paths ["java-src"])
