(ns raid.recipes
  (:refer-clojure :exclude [get])
  (:gen-class))

(defprotocol IRecipes
  (addRecipe [this action handler])
  (recipeByAction [this action])
  (recipes [this])
  (rmRecipe [this handler]))

(defn add [^raid.recipes.IRecipes io action handler]
  (.addRecipe io action handler))

(defn get [^raid.recipes.IRecipes io action]
  (.recipeByAction io action))

(defn rm [^raid.recipes.IRecipes io action]
  (.rmRecipe io action))
