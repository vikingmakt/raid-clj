(ns raid.udp
  (:require [raid.envelop :as envelop]
            [raid.handlers :as handlers]
            [raid.logging :as logging]
            [raid.promises :as promises]
            [raid.recipes :as recipes]
            [raid.resources :refer [byte-buffer-allocate byte-buffer-get
                                    byte-buffer-position byte-buffer-rewind
                                    byte-buffer-wrap handler-worker queue-put
                                    queue-take start-worker thread-interrupt uuid4
                                    try-close]]
            raid.socket)
  (:import [java.io PipedInputStream PipedOutputStream]
           java.net.InetSocketAddress
           java.nio.ByteBuffer
           [java.nio.channels DatagramChannel ClosedByInterruptException]
           java.util.concurrent.LinkedBlockingDeque)
  (:gen-class))

(def ^:const buf-size 512)

(defn- socket-read [^DatagramChannel socket ^ByteBuffer buf]
  (.receive socket buf))

(defn- read-buf [^DatagramChannel socket buf]
  (byte-buffer-rewind buf)
  (socket-read socket buf)
  (let [position (byte-buffer-position buf)]
    (when (pos? position)
      (byte-buffer-rewind buf)
      (byte-buffer-get buf position))))

(defn- socket-write [^DatagramChannel socket payload address]
  (doseq [part (partition-all buf-size payload)]
    (.send socket (byte-buffer-wrap (byte-array part)) address)))

(defn- feed-stream [^PipedOutputStream pipe-out reader]
  (.write pipe-out ^bytes (reader))
  (recur pipe-out reader))

(defn- read-socket [socket buf pipe-out status-state t-group]
  (try
    (feed-stream pipe-out #(read-buf socket buf))
    (catch ClosedByInterruptException e)))

(defn- handler-dispatch [io get-payload t]
  (handlers/on-msg io (get-payload))
  (recur io get-payload t))

(defn- write-socket [io socket queue address]
  (socket-write socket (queue-take queue) address)
  (recur io socket queue address))

(defn- recipes-get-by-action [r action]
  (get (recipes/recipes r) action))

(defn- socket-connect [^DatagramChannel socket ^Integer port]
  (.bind socket (new InetSocketAddress port)))

(defn- socket-close [^DatagramChannel socket status-state]
  (.close socket)
  (reset! status-state :CLOSED))

(defn- socket-get-remote-address [^DatagramChannel socket]
  (.getRemoteAddress socket))

(defn- socket-get-address [^DatagramChannel socket]
  (.getLocalAddress socket))

(defn- socket-open? [^DatagramChannel socket]
  (.isOpen socket))

(defn connect [^String host ^Integer port & [{:keys [decoder encoder encoder-delimiter stream-input local-port]
                                              :or {decoder envelop/utcode-unpack encoder envelop/utcode-pack}}]]
  (promises/start-routines)
  (let [uid (uuid4)
        c-name (str "raid-udp-" uid)
        socket (DatagramChannel/open)
        status-state (atom :NEW)
        c (atom -1)
        loggers (atom [])
        recipes (atom {})
        handlers (atom {})
        w-queue (new LinkedBlockingDeque)
        t-group (new ThreadGroup c-name)
        pipe-out (new PipedOutputStream)
        pipe-in (new PipedInputStream pipe-out)
        decoder-in (if stream-input (stream-input pipe-in) pipe-in)]
    (reify
      Object
      (toString [_]
        (if (socket-open? socket)
          (format "RAID UDP %s - OPEN %s <==> %s" uid (socket-get-address socket) (new InetSocketAddress host port))
          (format "RAID UDP %s - CLOSED" uid)))
      clojure.lang.Named
      (getName [_]
        c-name)
      raid.socket.ISocket
      (connect [this]
        (socket-connect socket (or local-port port))
        (reset! status-state :OPEN)
        (->> (read-socket socket (byte-buffer-allocate buf-size) pipe-out status-state t-group)
             (start-worker t-group (str "socket-reader:" (name this))))
        (->> (handler-dispatch this #(handler-worker decoder decoder-in) pipe-in)
             (start-worker t-group (str "handler-dispatch:" (name this))))
        (->> (write-socket this socket w-queue (new InetSocketAddress host port))
             (start-worker t-group (str "socket-writer:" (name this)))))
      (close [_]
        (thread-interrupt t-group)
        (socket-close socket status-state)
        (try-close decoder-in))
      (getAddress [_]
        (socket-get-remote-address socket))
      (status [_]
        @status-state)
      (statusAddListener [_ k cb]
        (add-watch status-state k cb))
      (statusRemoveListener [_ k]
        (remove-watch status-state k))
      (write [this msg]
        (queue-put w-queue (encoder msg))
        (logging/log-msg this :out msg))
      raid.handlers.IHandlers
      (addMsgHandler [_ handler]
        (let [id (swap! c inc)]
          (swap! handlers assoc id handler)
          id))
      (msgHandlers [_]
        (vals (deref handlers)))
      (rmMsgHandler [_ id]
        (swap! handlers dissoc id)
        true)
      raid.logging.ILogger
      (addLogger [_ handler]
        (swap! loggers conj handler))
      (getLoggers [_] @loggers)
      raid.recipes.IRecipes
      (addRecipe [_ action handler]
        (swap! recipes assoc action handler)
        true)
      (recipeByAction [this action]
        (recipes-get-by-action this action))
      (recipes [_]
        (deref recipes))
      (rmRecipe [_ action]
        (swap! recipes dissoc action)
        true))))
