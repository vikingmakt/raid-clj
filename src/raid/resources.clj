(ns raid.resources
  (:import [java.io InputStream InterruptedIOException IOException]
           java.nio.ByteBuffer
           java.security.MessageDigest
           [java.util.concurrent ExecutionException LinkedBlockingDeque]
           java.util.UUID)
  (:gen-class))

(defn datetime-float-str []
  (let [d (str (System/currentTimeMillis))]
    (str (subs d 0 10) "." (subs d 10 13))))

(defn- sha-digest [hash data]
  (.digest (MessageDigest/getInstance hash) data))

(defn sha-hash [hash ^String text]
  (->> (.getBytes text "UTF-8")
       (sha-digest hash)
       (map #(format "%x" %))
       (apply str)))

(defn uuid4 []
  (str (UUID/randomUUID)))

(defn gen-etag []
  (sha-hash "SHA-256" (format "[raid::etag-%s]" (uuid4))))

(defn queue-put [^LinkedBlockingDeque queue v]
  (.put queue v))

(defn queue-take [^LinkedBlockingDeque queue]
  (.take queue))

(defn byte-buffer-array [^ByteBuffer buf]
  (.array buf))

(defn byte-buffer-allocate [size]
  (ByteBuffer/allocate size))

(defn byte-buffer-get [^ByteBuffer buf size]
  (let [res (byte-array size)]
    (.get buf res)
    res))

(defn byte-buffer-position [^ByteBuffer buf]
  (.position buf))

(defn byte-buffer-rewind [^ByteBuffer buf]
  (.rewind buf))

(defn byte-buffer-wrap [^bytes payload]
  (ByteBuffer/wrap payload))

(defn thread-interrupt [^ThreadGroup t]
  (.interrupt t))

(defn start-worker* [^ThreadGroup t-group t-name callable]
  (doto (new Thread t-group callable t-name)
    .start))

(defn handler-worker [decoder decoder-in]
  (try
    (decoder decoder-in)
    (catch InterruptedIOException e
      {:stop true})
    (catch IOException e
      {:stop true})))

(defmacro start-worker [t-group t-name & body]
  `(start-worker*
    ~t-group
    ~t-name
    (fn []
      (try
        (do
          ~@body)
        (catch InterruptedException e#)
        (catch ExecutionException e#)))))

(defn try-close [^InputStream is]
  (try
    (.close is)
    (catch Exception e)))
