(ns raid.socket
  (:refer-clojure :exclude [close write])
  (:import java.net.InetSocketAddress)
  (:gen-class))

(defprotocol ISocket
  (connect [this])
  (close [this])
  (getAddress [this])
  (status [this])
  (statusAddListener [this k cb])
  (statusRemoveListener [this k])
  (write [this msg]))

(defn get-address [^raid.socket.ISocket io]
  (.getAddress io))

(defn get-host [^InetSocketAddress socket]
  (.getHostName socket))

(defn get-port [^InetSocketAddress socket]
  (.getPort socket))

(defn status-add-listener [^raid.socket.ISocket io k cb]
  (.statusAddListener io k cb))

(defn status-rm-listener [^raid.socket.ISocket io k]
  (.statusRemoveListener io k))
