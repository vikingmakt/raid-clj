(ns raid.core
  (:require [augustus.future :as afuture]
            [raid.handlers :as handlers]
            [raid.promises :as promises]
            [raid.resources :refer [gen-etag sha-hash uuid4]]
            [raid.server :as server]
            [raid.socket :as socket]
            [raid.tcp :as tcp]
            [raid.udp :as udp])
  (:gen-class))

(def default-timeout 5000)

(defn- default-handler [io msg]
  (let [etag (get-in msg [:header :etag])
        prom (promises/get-promise etag)]
    (if prom
      (promises/promise-resolve prom msg))))

(defn- push-msg [headers action etag body]
  (merge
   {:header (assoc headers
                   :action action
                   :etag etag)}
   (if body {:body body})))

(defn close [io]
  (socket/close io))

(defn push [io action etag headers body & [delimiter]]
  (socket/write io (push-msg headers action etag body)))

(defn request [io action & [body headers {:keys [timeout delimiter] :or {timeout default-timeout}}]]
  (let [etag (gen-etag)
        fut (afuture/new-future)]
    (promises/add-promise etag fut :timeout timeout)
    (push io action etag headers body delimiter)
    fut))

(defn connect-tcp [host port & [opt]]
  (doto (tcp/connect host port opt)
    socket/connect
    (handlers/add default-handler)))

(defn connect-udp [host port & [opt]]
  (doto (udp/connect host port opt)
    socket/connect
    (handlers/add default-handler)))

(defn server-tcp [port & [opt]]
  (doto (tcp/open-server port opt)
    server/bind))
