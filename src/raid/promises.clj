(ns raid.promises
  (:refer-clojure :exclude [resolve promise])
  (:require [augustus.future :as afuture]
            [augustus.timed :as timed])
  (:import java.util.Calendar
           [java.util.concurrent Executors TimeoutException])
  (:gen-class))

(def default-timeout 5000)

(def ^:private promises (atom {}))
(def ^:private routine-running (atom false))

(defprotocol IPromise
  (cancel [this])
  (date [this])
  (isExpired [this] [this now])
  (resolve [this result]))

(defn promise-expired? [^raid.promises.IPromise prom & [now]]
  (if now
    (.isExpired prom now)
    (.isExpired prom)))

(defn promise-resolve [^raid.promises.IPromise prom result]
  (.resolve prom result))

(defn promise [etag fut & {:keys [timeout]}]
  (let [d (Calendar/getInstance)
        e (.clone d)]
    (.add ^Calendar e Calendar/MILLISECOND (or timeout default-timeout))
    (reify IPromise
      (cancel [_]
        (swap! promises dissoc etag)
        (afuture/cancel fut))
      (date [_]
        (.getTime d))
      (isExpired [this]
        (.isExpired this (Calendar/getInstance)))
      (isExpired [_ now]
        (.before ^Calendar e now))
      (resolve [_ result]
        (swap! promises dissoc etag)
        (try
          (do
            (fut result)
            true)
          (catch Throwable e
            false))))))

(defn add-promise [etag fut & {:keys [timeout]}]
  (let [prom (promise etag fut :timeout timeout)]
    (swap! promises assoc etag prom)
    prom))

(defn get-promise [etag]
  (get @promises etag))

(defn- check-timedout []
  (let [now (Calendar/getInstance)]
    (doseq [promise (vals @promises)]
      (if (promise-expired? promise now)
        (promise-resolve promise (new TimeoutException))))))

(defn start-routines []
  (when (compare-and-set! routine-running false true)
    (doto (Executors/newScheduledThreadPool 1)
      (timed/custom-add-delay-coroutine* 1000 check-timedout))))
