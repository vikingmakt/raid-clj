(ns raid.tcp
  (:require [raid.envelop :as envelop]
            [raid.handlers :as handlers]
            [raid.logging :as logging]
            [raid.promises :as promises]
            [raid.recipes :as recipes]
            [raid.resources :refer [byte-buffer-allocate byte-buffer-array
                                    byte-buffer-wrap queue-put queue-take
                                    start-worker thread-interrupt uuid4]]
            raid.server
            [raid.socket :as socket])
  (:import [java.io IOException InterruptedIOException]
           java.net.InetSocketAddress
           java.nio.ByteBuffer
           [java.nio.channels ClosedChannelException SocketChannel ServerSocketChannel]
           [java.util.concurrent ExecutionException LinkedBlockingDeque]
           [raid.exceptions EncodeInvalid RemoteConnectionClosed])
  (:gen-class))

(def ^:const buf-header-size 4)
(def ^:const buf-size 512)

(defn- socket-read [^SocketChannel socket ^ByteBuffer buf]
  (let [bytes-read (.read socket buf)]
    (if (neg? bytes-read)
      (throw (new IOException "Socket read closed")))
    bytes-read))

(defn- get-buf-size [^SocketChannel socket header-size]
  (let [buf-header (byte-buffer-allocate header-size)]
    (socket-read socket buf-header)
    (byte-buffer-array buf-header)
    (bigint (byte-array (cons 0 (byte-buffer-array buf-header))))))

(defn- read-buf* [socket buf-size msg-size]
  (if (pos? msg-size)
    (let [buf (byte-buffer-allocate (if (> msg-size buf-size) buf-size msg-size))
          position (socket-read socket buf)]
      (if (pos? position)
        (lazy-seq (concat (byte-buffer-array buf) (read-buf* socket buf-size (- msg-size buf-size))))
        (throw (new RemoteConnectionClosed))))))

(defn- read-buf [socket header-size buf-size]
  (let [msg-size (get-buf-size socket header-size)]
    (byte-array (read-buf* socket buf-size msg-size))))

(defn- gen-payload-size [buf-size msg-size]
  (byte-array (take-last buf-size (concat (byte-array buf-size) (.toByteArray (BigInteger/valueOf msg-size))))))

(defn- socket-write [^SocketChannel socket payload delaying]
  (let [payload-size (gen-payload-size buf-header-size (count payload))]
    (.write socket ^ByteBuffer (byte-buffer-wrap payload-size))
    (doseq [part (partition-all buf-size payload)]
      (.write socket ^ByteBuffer (byte-buffer-wrap (byte-array part)))
      (Thread/sleep delaying))))

(defn- parse-execution-exception
  ([status-state e t-group]
   (parse-execution-exception status-state e t-group (:via (Throwable->map e))))
  ([status-state e t-group exl]
   (if exl
     (if (isa? (-> exl first :type) IOException)
       (reset! status-state :REMOTE-CLOSED)
       (recur status-state e t-group (next exl)))
     (throw e))))

(defn- read-socket* [socket buf-header-size buf-size r-queue]
  (queue-put r-queue (read-buf socket buf-header-size buf-size))
  (recur socket buf-header-size buf-size r-queue))

(defn- read-socket [socket buf-header-size buf-size r-queue status-state t-group]
  (try
    (read-socket* socket buf-header-size buf-size r-queue)
    (catch ClosedChannelException e
      (reset! status-state :REMOTE-CLOSED))
    (catch RemoteConnectionClosed e
      (reset! status-state :REMOTE-CLOSED))
    (catch ExecutionException e
      (parse-execution-exception status-state e t-group))
    (catch IOException e
      (parse-execution-exception status-state e t-group))
    (catch InterruptedException e)))

(defn- handler-dispatch* [io queue decoder]
  (if (handlers/on-msg io (decoder (queue-take queue)))
    (recur io queue decoder)))

(defn- handler-dispatch [io queue decoder status-state t-group]
  (try
    (handler-dispatch* io queue decoder)
    (catch ClosedChannelException e
      (reset! status-state :REMOTE-CLOSED))
    (catch RemoteConnectionClosed e
      (reset! status-state :REMOTE-CLOSED))
    (catch ExecutionException e
      (parse-execution-exception status-state e t-group))
    (catch IOException e
      (parse-execution-exception status-state e t-group))
    (catch InterruptedException e)))

(defn- write-socket [io socket queue delaying]
  (socket-write socket (queue-take queue) delaying)
  (recur io socket queue delaying))

(defn- recipes-get-by-action [r action]
  (get (recipes/recipes r) action))

(defn- socket-connect [^SocketChannel socket ^String host ^Integer port]
  (.connect socket (new InetSocketAddress host port)))

(defn- socket-close [^SocketChannel socket status-state]
  (.close socket)
  (reset! status-state :CLOSED))

(defn- socket-get-remote-address [^SocketChannel socket]
  (.getRemoteAddress socket))

(defn- socket-get-address [^SocketChannel socket]
  (list (.getLocalAddress socket) (socket-get-remote-address socket)))

(defn- socket-open? [^SocketChannel socket]
  (.isOpen socket))

(defn- server-accept [^ServerSocketChannel server]
  (.accept server))

(defn- new-socket [socket host port & [{:keys [encoding decoder encoder delay-write]
                                        :or {decoder envelop/utcode-unpack encoder envelop/utcode-pack delay-write 5}} initial-status recipes handlers loggers]]
  (promises/start-routines)
  (let [{:keys [encoder decoder]} (if-let [encoding (get envelop/encoding-list encoding)]
                                    encoding
                                    {:encoder encoder :decoder decoder})
        uid (uuid4)
        c-name (str "raid-tcp-" uid)
        status-state (atom (or initial-status :NEW))
        c (atom -1)
        loggers (or loggers (atom []))
        recipes (or recipes (atom {}))
        handlers (or handlers (atom {}))
        w-queue (new LinkedBlockingDeque)
        r-queue (new LinkedBlockingDeque)
        t-group (new ThreadGroup c-name)
        io (reify
             Object
             (toString [_]
               (if (socket-open? socket)
                 (apply format "RAID TCP %s - OPEN %s <==> %s" uid (socket-get-address socket))
                 (format "RAID TCP %s - CLOSED" uid)))
             clojure.lang.Named
             (getName [_]
               c-name)
             raid.socket.ISocket
             (connect [this]
               (case @status-state
                 :NEW
                 (do
                   (socket-connect socket host port)
                   (->> (read-socket socket buf-header-size buf-size r-queue status-state t-group)
                        (start-worker t-group (str "socket-reader:" (name this))))
                   (->> (handler-dispatch this r-queue decoder status-state t-group)
                        (start-worker t-group (str "handler-dispatch:" (name this))))
                   (->> (write-socket this socket w-queue delay-write)
                        (start-worker t-group (str "socket-writer:" (name this))))
                   (reset! status-state :OPEN))))
             (close [_]
               (socket-close socket status-state)
               (thread-interrupt t-group))
             (getAddress [_]
               (socket-get-remote-address socket))
             (status [_]
               @status-state)
             (statusAddListener [_ k cb]
               (add-watch status-state k cb))
             (statusRemoveListener [_ k]
               (remove-watch status-state k))
             (write [this msg]
               (if-not (= @status-state :OPEN)
                 (throw (new RemoteConnectionClosed (str "Connection with the server is" (name @status-state)))))
               (queue-put w-queue (encoder msg))
               (logging/log-msg this :out msg))
             raid.handlers.IHandlers
             (addMsgHandler [_ handler]
               (let [id (swap! c inc)]
                 (swap! handlers assoc id handler)
                 id))
             (msgHandlers [_]
               (vals (deref handlers)))
             (rmMsgHandler [_ id]
               (swap! handlers dissoc id)
               true)
             raid.logging.ILogger
             (addLogger [_ handler]
               (swap! loggers conj handler))
             (getLoggers [_] @loggers)
             raid.recipes.IRecipes
             (addRecipe [_ action handler]
               (swap! recipes assoc action handler)
               true)
             (recipeByAction [this action]
               (recipes-get-by-action this action))
             (recipes [_]
               (deref recipes))
             (rmRecipe [_ action]
               (swap! recipes dissoc action)
               true))]
    (when (= @status-state :OPEN)
      (->> (read-socket socket buf-header-size buf-size r-queue status-state t-group)
           (start-worker t-group (str "socket-reader:" (name io))))
      (->> (handler-dispatch io r-queue decoder status-state t-group)
           (start-worker t-group (str "handler-dispatch:" (name io))))
      (->> (write-socket io socket w-queue delay-write)
           (start-worker t-group (str "socket-writer:" (name io)))))
    (socket/status-add-listener io :default (fn [_ _ _ new-val]
                                              (case new-val
                                                :REMOTE-CLOSED
                                                (do
                                                  (socket/status-rm-listener io :default)
                                                  (socket/close io))
                                                nil)))
    io))

(defn- server-on-socket-status-changed [clients client key ref old-val new-val]
  (when (some #{new-val} [:CLOSED :REMOTE-CLOSED])
    (swap! clients #(filterv (complement #{client}) %))))

(defn- accept-connection [server clients recipes handlers loggers opt]
  (while true
    (let [io (server-accept server)]
      (if (and io (socket-open? io))
        (let [add (socket-get-remote-address io)
              client (new-socket io (socket/get-host add) (socket/get-port add) opt :OPEN recipes handlers loggers)]
          (socket/status-add-listener client :server (fn [& args] (apply server-on-socket-status-changed clients client args)))
          (swap! clients conj client))))))

(defn connect [host port & [{:keys [encoding decoder encoder delay-write]
                             :or {decoder envelop/utcode-unpack encoder envelop/utcode-pack delay-write 5}
                             :as opt}]]
  (if (and encoding (not (some #{encoding} (keys envelop/encoding-list))))
    (throw (new EncodeInvalid (str encoding))))
  (new-socket (SocketChannel/open) host port opt))

(defn open-server [^Integer port & [{:keys [encoding decoder encoder delay-write]
                                     :or {decoder envelop/utcode-unpack encoder envelop/utcode-pack delay-write 5}
                                     :as opt}]]
  (if (and encoding (not (some #{encoding} (keys envelop/encoding-list))))
    (throw (new EncodeInvalid (str encoding))))
  (promises/start-routines)
  (let [uid (uuid4)
        c-name (str "raid-server-tcp-" uid)
        server (ServerSocketChannel/open)
        status-state (atom :NEW)
        clients (atom [])
        c (atom -1)
        loggers (atom [])
        recipes (atom {})
        handlers (atom {})
        t-group (new ThreadGroup c-name)]
    (reify
      Object
      (toString [_]
        (format "RAID SERVER TCP %s - WAITING - PORT: %s" uid port))
      clojure.lang.Named
      (getName [_]
        c-name)
      raid.server.IServer
      (bind [this]
        (.bind server (new InetSocketAddress port))
        (reset! status-state :WAITING)
        (->> (accept-connection server clients recipes handlers loggers opt)
             (start-worker t-group (str "socket-accept:" (name this)))))
      (clients [_]
        (deref clients))
      (clients-add-listener [_ k cb]
        (add-watch clients k cb))
      (clients-rm-listener [_ k]
        (remove-watch clients k))
      (close [_]
        (thread-interrupt t-group)
        (doseq [client clients]
          (socket/close client))
        (.close server)
        (reset! status-state :CLOSED))
      (status [_]
        @status-state)
      (status-add-listener [_ k cb]
        (add-watch status-state k cb))
      (status-rm-listener [_ k]
        (remove-watch status-state k))
      raid.handlers.IHandlers
      (addMsgHandler [_ handler]
        (let [id (swap! c inc)]
          (swap! handlers assoc id handler)
          id))
      (msgHandlers [_]
        (vals (deref handlers)))
      (rmMsgHandler [_ id]
        (swap! handlers dissoc id)
        true)
      raid.logging.ILogger
      (addLogger [_ handler]
        (swap! loggers conj handler))
      (getLoggers [_] @loggers)
      raid.recipes.IRecipes
      (addRecipe [_ action handler]
        (swap! recipes assoc action handler)
        true)
      (recipeByAction [this action]
        (recipes-get-by-action this action))
      (recipes [_]
        (deref recipes))
      (rmRecipe [_ action]
        (swap! recipes dissoc action)
        true))))
