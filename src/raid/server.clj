(ns raid.server
  (:refer-clojure :exclude [close write])
  (:import java.net.InetSocketAddress)
  (:gen-class))

(defprotocol IServer
  (bind [this])
  (clients [this])
  (clients-add-listener [this k cb])
  (clients-rm-listener [this k])
  (close [this])
  (status [this])
  (status-add-listener [this k cb])
  (status-rm-listener [this k]))
