(ns raid.logging
  (:require [augustus.async :as async])
  (:import [java.util.concurrent Executors ExecutorService ThreadFactory])
  (:gen-class))

(defn- set-thread-name [^Thread t c]
  (.setName t (str "logging-thread-" (swap! c inc))))

(defn- factory []
  (let [c (atom 0)]
    (proxy [ThreadFactory] []
      (newThread [^Runnable r]
        (doto (new Thread r)
          (set-thread-name c))))))

(defonce ^ExecutorService pool (Executors/newSingleThreadExecutor (factory)))

(defprotocol ILogger
  (addLogger [this handler])
  (getLoggers [this]))

(defn add-logger [^raid.logging.ILogger io handler]
  (.addLogger io handler))

(defn get-loggers [^raid.logging.ILogger io]
  (.getLoggers io))

(defn log-msg* [io loggers t msg]
  (when (seq loggers)
    (try
      ((first loggers) t msg)
      (catch Exception e))
    (recur io (rest loggers) t msg)))

(defn log-msg
  ([io t msg]
   (log-msg io (get-loggers io) t msg))
  ([io loggers t msg]
   (async/custom-execute pool (log-msg* io loggers t msg))))
