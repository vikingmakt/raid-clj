(ns raid.envelop
  (:require [clojure.string :as string]
            [msgpack.core :as msgpack]
            [raid.resources :refer [datetime-float-str gen-etag uuid4]]
            [utcode.core :as utcode])
  (:import [java.io InputStream IOException]
           raid.exceptions.ActionMissing)
  (:gen-class))

(defn pack-action [p]
  (if-not (get-in p [:header :action])
    (throw (new ActionMissing))
    p))

(defn pack-etag [p]
  (if-not (get-in p [:header :etag])
    (update-in p [:header :etag] (fn [_] (gen-etag)))
    p))

(defn pack-sysdate [p]
  (if-not (contains? p :__sysdate__)
    (update-in p [:header :__sysdate__] (fn [_] (datetime-float-str)))
    p))

(defn bytes->string [^bytes v]
  (new String v))

(defn instance-byte? [v]
  (= (type v) (Class/forName "[B")))

(defn parse-str [v]
  (if (instance-byte? v)
    (bytes->string v)
    v))

(defn field-byte? [v]
  (string/starts-with? v "b_"))

(defn convert->unpack [value]
  (cond
    (map? value)
    (into {} (for [[k v] value]
               (let [k (parse-str k)]
                 [(keyword k) (if (field-byte? k) v (convert->unpack v))])))
    (sequential? value) (mapv convert->unpack value)
    :else
    (parse-str value)))

(defn convert->pack [value]
  (cond
    (map? value) (into {} (for [[k v] value] [(name k) (convert->pack v)]))
    (sequential? value) (mapv convert->pack value)
    (keyword? value) (name value)
    :else
    value))

(defn str->bytes [^String s]
  (.getBytes s "UTF-8"))

(defn bytes-append [data more-data]
  (byte-array (concat data more-data)))

(defmacro thread-envelop [& body]
  (do ~@body))

(defn utcode-pack [p]
  (-> p
      pack-action
      pack-etag
      pack-sysdate
      utcode/encode
      str->bytes))

(defn utcode-unpack [p]
  (try
    {:ok true :msg (utcode/decode p)}
    (catch IOException e
      (throw e))
    (catch Exception e
      {:ok false :err e})))

(defn msgpack-pack [p]
  (-> p
      pack-action
      pack-etag
      pack-sysdate
      convert->pack
      msgpack/pack))

(defn msgpack-unpack [p]
  (try
    {:ok true :msg (-> p msgpack/unpack convert->unpack)}
    (catch IOException e
      (throw e))
    (catch Exception e
      {:ok false :err e})))

(def ^:const encoding-list
  {:utcode {:encoder utcode-pack
            :decoder utcode-unpack}
   :msgpack {:encoder msgpack-pack
             :decoder msgpack-unpack}})
