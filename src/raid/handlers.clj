(ns raid.handlers
  (:refer-clojure :exclude [get remove])
  (:require [augustus.async :as async]
            [raid.logging :as logging]
            [raid.recipes :as recipes])
  (:gen-class))

(defprotocol IHandlers
  (addMsgHandler [this handler])
  (msgHandlers [this])
  (rmMsgHandler [this id]))

(defn add [^raid.handlers.IHandlers io handler]
  (.addMsgHandler io handler))

(defn get [^raid.handlers.IHandlers io]
  (.msgHandlers io))

(defn remove [^raid.handlers.IHandlers io id]
  (.rmMsgHandler io id))

(defn- call-handlers [handlers io msg]
  (when handlers
    (async/add-callback (first handlers) io msg)
    (recur (next handlers) io msg)))

(defn on-msg [io payload]
  (let [{:keys [ok msg stop]} payload]
    (when-not stop
      (if ok
        (let [action (get-in msg [:header :action])
              recipe (recipes/get io action)]
          (if recipe
            (call-handlers [recipe] io msg)
            (call-handlers (get io) io msg))
          (logging/log-msg io :in msg)))
      true)))
