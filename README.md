# RAID Client

## INSTALL

[![Clojars Project](https://img.shields.io/clojars/v/br.com.vikingmakt/raid.svg)](https://clojars.org/br.com.vikingmakt/raid)

## HOW TO CONNECT TO A SERVER

```clojure
(require 'raid.core)

(def connection (raid.core/tcp-connect "localhost" 8888))
```

OR

```clojure
(def connection (raid.core/udp-connect "localhost" 8888 {:local-port 8890}))
```

### REQUESTING

```clojure
(let [resp @(raid.core/request connection "utos.es.api.utos.timestamp")]
  (println resp))
```

```clojure
>>> {:body 1517507374
     :header {:code "UTOS-00000"
              :etag "f01fea9b4be6de12f0f05adb92fcd33e6ef2a9cba258c460c7d32c18df54"}}
```

## HOW TO START A RAID SERVER (JUST TCP FOR NOW)

```clojure
(require 'raid.core)
(require 'raid.handlers)
(require 'raid.recipes)
(require 'raid.server)
(require 'raid.socket)

(defn clients-logs [key ref old-val new-val]
  (let [[closing opening _] (diff old-val new-val)]
    (doseq [client opening]
      (println (format "Connected to %s" client)))
    (doseq [client closing]
      (println (format "Closed connection with %s" client)))))

(defn my-custom-handler [client msg]
  (raid.socket/write client (assoc msg :body "I was not expecting you here")))

(defn sum-some-number [client msg]
  (let [{:keys [x y]} (:body msg)]

    (raid.socket/write client (update-in msg [:body] (fn [body] (assoc body :result (+ x y)))))))

(let [server (raid.core/server-tcp 8088 {:encoding :msgpack})]
  (raid.server/clients-add-listener server :my-tcp-server clients-logs)
  (raid.handlers/add server my-custom-handler)
  (raid.recipes/add server "i.want.to.sum.numbers" sum-some-number))
```

## INCLUDING ANOTHER MESSAGE FORMAT

The raid client/server support the formats `:utcode` and `:msgpack` (default is `:utcode`):

```clojure
(let [connection (raid/connect-tcp
                   hostname port
                   {:encoding :msgpack})]
  ...
```

If you need anything else, you can add a function to encode and a function to decode the messages:

```clojure
(let [connection (raid/connect-tcp
                   hostname port
                   {:decoder unpack
                    :encoder pack
                    :stream-input #(new DataInputStream %)})]
  ...
```

The unpacked message must have the keys `:ok`, telling if the payload was decoded, and `:msg`, which is the decoded message, and `:err`, with the exception, or throw a `IOException` to tell the decoder thread to stop.

```clojure
{:ok true :msg {:header {:code "UTOS-00000" :etag "123456789"}}}
```

### JSON

An example of json pack and unpack functions:

```clojure
(ns example.core
  (:require [cheshire.core :as cheshire]
            [msgpack.core :as msgpack]
            ;; Let's import the functions to validate and complete the message
            [raid.envelop :refer [pack-action pack-etag pack-sysdate str->bytes]])
  (:gen-class))

(defn json-pack [p & [delimiter]]
  (-> p
      pack-action
      pack-etag
      pack-sysdate
      cheshire/generate-string
      (str delimiter)
      str->bytes))

(defn json-unpack [^InputStream s]
  (try
    {:ok true :msg (cheshire/parse-stream s true)}
    (catch IOException e
      {:stop true})
    (catch Exception e
      {:ok false :err e})))

```

# CHANGELOG

## [1.5.10]

* Delaying write to socket (default to 5ms)

## [1.5.9]

* Changing variables
* Partitioning write to socket by 512b
* Changing buffer size to 512b

## [1.5.8]

* Flushing piped output stream after writing

## [1.5.7]

* Fix infinity loop when connection is closed

## [1.5.6]

* Remove thread pool from envelop

## [1.5.5]

* Fix TCP client when default params used
* Fix UDP write method
* Set threads for envelop to 10 (before as 2)

## [1.5.4]

* Send message with delimiter

## [1.5.3]

* TCP server

## [1.5.2]

* Clients listeners
* Handlers hierarchy
* Status listeners

## [1.5.1]

* TCP server

## [1.4.0]

* Adding UDP support

## [1.3.1]

* Streamming reading

## [1.0.0]

* Rewrite TCP client
* Writing multiple messages once
* Fix socket reader discarting messages if server writes too fast
* Remove UDP client (temporary)

## [0.3.8]

* Fix Broken Pipe exception when trying to write on a closed socket

## [0.3.7]

* Queue TCP socket write

## [0.3.6]

* Using augustus lib

## [0.3.5]

* Recipes

## [0.3.4]

* Bug fix - wrong function positioning

## [0.3.3]

* Wait TCP connection to be ready

## [0.3.2]

* Removing NO_DELAY option due Java socket bug

## [0.3.1]

* Bug fix - breakline by message

## [0.3.0]

* Changing TCP lib to java.nio.
* Rewrite thread pool call and thread pool as callback

## [0.2.3]

* Using thread pool to async operations instead of common threads

## [0.2.2]

* Logging handlers

## [0.2.1]

* Using defmulti to call shared functions
* Function "request" created

## [0.2.0]

* Connection object as record
* Separate handlers for messages and exceptions

## [0.1.1]

* Remove "connect" function from tcp/udp
* Base functions for common operations

## [0.0.3]

* Thread start function
* Using macros instead of lambdas

## [0.0.2]

* Some recipes and cancel socket listening

## [0.0.1]

* Socket creation and socket listening
